//
//  COLayerGame.h
//  ICommandos
//
//  Created by Noriaf on 06/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CCLayer.h"
#import "cocos2d.h"
//importamos nuestra clase mapa
#import "COMapa.h"
//para mover por el mapa

#import "CCPanZoomController.h"

#import "COControlador.h"



@interface COLayerGame : CCLayer

{
    COMapa *tileMap;
    CCSprite *player;
    CCPanZoomController *control;
    CCSpriteBatchNode *sceneBatchNode;
    
}





@end