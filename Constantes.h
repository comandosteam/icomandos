//
//  Constantes.h
//  ICommandos
//
//  Created by Noriaf on 08/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef ICommandos_Constantes_h
#define ICommandos_Constantes_h

//AYUDA PARA LA DEFINICION DE ENEMIGOS

 

typedef enum
{
    kPosPlayer=1,
    kPosEnemigo=0,
    kPosMapa=0
    
}kPosCapaObjetos;

typedef enum
{
    kTipoObjetoRadar=0,
    kTipoObjetoMapa=1
    
}kTipoObjeto;


typedef enum
{
    kArmaMachete=0,
    kArmaPistola=1
    
}kTipoArma;


typedef enum
{
    kTipoMapaBomba=0,
    kTipoMapaRescate=1
    
}kTipoMapa;


typedef enum
{
    kTipoEnemigoSoldado=0
}kTipoEnemigo;

typedef enum
{
    kTipoHeroeBoina=0
}kTipoHeroe;


#endif
