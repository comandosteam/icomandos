//
//  COEnemigo.m
//  ICommandos
//
//  Created by Noriaf on 29/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "COEnemigo.h"
#import "cocos2d.h"




@implementation COEnemigo


@synthesize  rutaPatrulla;
@synthesize  tipoEnemigo;

//ayuda para el movimiento y mapa

 

//radar de la vision
 

//se le asigna el mapa que tiene
 
 
 

-(id) initWithDiccionario:(NSMutableDictionary*) DiccionarioEnemigo


{
     //se busca el sprite, coordenada de origen,tipo de enemigo, (animaciones), ruta de patrulla
    self=[super initWithFile:[DiccionarioEnemigo valueForKey:@"Sprite"]];
          
    if(self!=nil)
        
    {
        
        
        //se ponen las caracteristicas del mapa donde se mueve el enemigo
        //con self parent recupera el mapa y setea los valores correspondientes
        
        
      
        
      
        
        CGPoint puntoOrigen;
        
        //tipo de enemigo
        self.tipoEnemigo=  [[DiccionarioEnemigo valueForKey:@"TipoEnemigo" ] intValue];
        //rutaParala patruella
        self.rutaPatrulla=[DiccionarioEnemigo valueForKey:@"polylinePoints"];
        //punto de origen del enemigo                   
        puntoOrigen.x=[[DiccionarioEnemigo valueForKey:@"x"] floatValue];
        puntoOrigen.y=[[DiccionarioEnemigo valueForKey:@"y"] floatValue];
        puntoOrigen=[[[COControlador sharedControlador] mapaActual] ajustarPuntoCentroTilset:puntoOrigen];
        
        
        
        self.position=puntoOrigen;
         
       
        
        
       
        
        

        
        
        
       // [self addChild:radar z:kPosEnemigo tag:kTipoObjetoRadar];
        
    
        
        
        //por defecto no esta seleccionado
        isSeleccionado=NO;
        //angulo de correccion del sprite
        anguloSprite=180;
        
        if([DiccionarioEnemigo valueForKey:@"Angulo" ])
            
        {
            self.rotation=[[DiccionarioEnemigo valueForKey:@"Angulo" ] intValue]+anguloSprite;
            
            
        } 
        
         
        //velocidad por defecto del sprite
      
        if([DiccionarioEnemigo valueForKey:@"Velocidad" ])
            
        {
            velocidad=[[DiccionarioEnemigo valueForKey:@"Velocidad" ] intValue];
            
            
        }else {
            velocidad=90;
        }
            
        
        //se crea la animacion de la patrulla si se mueve
       if(velocidad!=0)
       {
        [self Patrullar];
       }
 
        
         //personaliza dependiendo del tipo de enemigo
        
        switch (self.tipoEnemigo) {
            case kTipoEnemigoSoldado:
                //la vision es de 3 casillas;
                
                vision.size.width=2*32;
                vision.size.height=3*32;
                vision.origin=ccp(0,0);
                vision.origin.x+=32;
                vision.origin.y-=32;
                
                
                break;
                
            default:
                break;
        }
        
        
 

           
    }
 
    return self;
  
}




////////// FUNCIONES PARA EL RADAR, SE PASA A UNA CLASE RADAR
-(void) actualizarRadar
{
    
     
        
        
    //[radar dibujarRadar];
    
    
         
}
 
  
 -(void) draw
{
     [super draw];
    
   
    
    //dibujamos el marco de la vision
  /*  CGPoint vertices[4]=
    {
        vision.origin,
        ccp(vision.origin.x,vision.origin.y+vision.size.height),
        ccp(vision.origin.x+vision.size.width,vision.origin.y+vision.size.height),
        ccp(vision.origin.x+vision.size.width,vision.origin.y)
        
        
        
        
    };*/
    
    //convierto los vertices a la capa
    
    
    
    
     
    

    
    
    
    
    CGPoint visionOrigen;
    CGPoint visionDestino;
    
    visionOrigen=self.position;
    //el punto destino se rota con el sprite
    
    
     
    visionDestino=ccpAdd(visionOrigen, ccp(32*3 ,0 ));
    visionDestino=  ccpRotateByAngle(visionDestino, visionOrigen, CC_DEGREES_TO_RADIANS( -self.rotation  ));
    
    CGPoint destinoConvertido,origenConvertido;
    
    origenConvertido=[self convertToNodeSpace:visionOrigen];
    destinoConvertido=[self convertToNodeSpace:visionDestino];
    
   
    //[self dibujarRayCast:origenConvertido puntoDestino:destinoConvertido angulo:90];
    
    NSMutableArray* objetos=[[[COControlador sharedControlador] mapaActual] objetosAnguloVisionOrigen:self.position puntoDestino:visionDestino angulo:90];
    
    
    
    
    CGPoint vertices[objetos.count+1];
    ;
    vertices[0]=origenConvertido;
    
    
      for(int i=0;i<objetos.count;i++)
    {
    
          
         CGPoint puntoMedio=[[objetos objectAtIndex:i] CGPointValue];
         
        //destinoConvertido=ccpSub(self.position, puntoMedio);
        //origenConvertido=ccpSub(self.position, visionOrigen);
        puntoMedio=[self convertToNodeSpace:puntoMedio];
       
        
        
      //    ccDrawLine( origenConvertido, puntoMedio);
        vertices[i+1]=puntoMedio;
        
   
     }
    
    // [self pintarPoligono:vertices : objetos.count+1:YES];
     //ccDrawPoly(vertices, objetos.count+1, YES);
    
    CCLOG(@"vision: %@",objetos);
    
 
    
    [self obtenerVertices:objetos puntoOrigen:self.position];
    

}

-(void) obtenerVertices : (NSMutableArray *) puntosMediosCuadrados puntoOrigen:(CGPoint) puntoOrigen

{

     
    CGRect cuadrado;
    CGPoint puntoMedioCuadrado,puntoOrigenCuadrado,puntoAnterior,puntoSiguiente;
    float  TilsetX=[[COControlador sharedControlador] mapaActual].tileWidh ;
    float  TilsetY=[[COControlador sharedControlador] mapaActual].tileHeight ;
    CGPoint calculoOrigen=ccp(- TilsetX*0.5,- TilsetY*0.5);

    NSMutableArray *verticesUtiles=[[NSMutableArray alloc] init];
    
    float pasoPunto=4;
    
    CGRect cuadrados[puntosMediosCuadrados.count];
    
    for (int i=0;i<puntosMediosCuadrados.count;i++)
    {
        
        
        //recupero el cuadrado a analizar
        puntoMedioCuadrado=[[puntosMediosCuadrados objectAtIndex:i] CGPointValue];
        puntoOrigenCuadrado=ccpAdd(puntoMedioCuadrado, calculoOrigen);
        cuadrado=CGRectMake(puntoOrigenCuadrado.x, puntoOrigenCuadrado.y, TilsetX, TilsetY);
        //añadimos en el array de cuadrados para su posterior comprobacion
        cuadrados[i]=cuadrado;
        
        
        //calculamos los vertices
        //
        //     B-----A
        //     |     |
        //     |     |
        //     C-----D
        //
        
        CGPoint verticeA=ccp(puntoOrigenCuadrado.x+cuadrado.size.width,puntoOrigenCuadrado.y+cuadrado.size.height);
        CGPoint verticeB=ccp(puntoOrigenCuadrado.x,puntoOrigenCuadrado.y+cuadrado.size.height);
        CGPoint verticeC=puntoOrigenCuadrado;
        CGPoint verticeD=ccp(puntoOrigenCuadrado.x+cuadrado.size.width,puntoOrigenCuadrado.y);
        
        //nos quedamos con los vertices que el siguiente punto esta dentro del cuadrado o el anterior esta fuera
        //sigueinte
        
        CGPoint verticesCuadrado[4]={verticeA,verticeB,verticeC,verticeD};
        
        
        for(int i=0;i<4;i++)
        {
            
            BOOL siguienteDentro=false;
            BOOL anteriorDentro=false;
            
            CGPoint puntoComprobar=[self puntoEnLinea:puntoOrigen puntoB:verticesCuadrado[i] punto:pasoPunto siguiente:YES];
            siguienteDentro=CGRectContainsPoint(cuadrado, puntoComprobar);
            puntoComprobar=[self puntoEnLinea:puntoOrigen puntoB:verticesCuadrado[i] punto:pasoPunto siguiente:NO];
            anteriorDentro=CGRectContainsPoint(cuadrado, puntoComprobar);
            
            if(siguienteDentro || !anteriorDentro)
            {
                //vertice para la huchaca!!!
                 [verticesUtiles addObject:[NSValue valueWithCGPoint:verticesCuadrado[i]]];
          
            }
 
            
        }
        
       //depuramos los vertices utilies, eliminamos los vertices que el punto anterior este incluido en algun cuadrado
        
                    
    /*        for(int verticeComprobar=0;verticeComprobar<verticesUtiles.count;verticeComprobar++)
                
                
            {
                CGPoint puntoComprobar=[[verticesUtiles objectAtIndex:verticeComprobar] CGPointValue];
                CGPoint puntoAnterior=[self puntoEnLinea:puntoOrigen puntoB:puntoComprobar punto:pasoPunto siguiente:NO];
                BOOL anteriorDentro=false;
                
                int numCuadradoComprobar=0;
                
                while (numCuadradoComprobar<puntosMediosCuadrados.count && !anteriorDentro) {
                    
                    
                    CGRect cuadradoComprobar=cuadrados[i];
                    anteriorDentro= CGRectContainsPoint(cuadradoComprobar, puntoAnterior);
                    numCuadradoComprobar++;
                    
                }                 
                
                //si esta incluido se debe eliminar ese punto
                if(anteriorDentro)
                {
                    [verticesUtiles removeObjectAtIndex:verticeComprobar];
                    
                    
                }
                
                
                
            }*/
            
            
         
        
        
        
        
        
      
        CGPoint verticesDibujar[verticesUtiles.count];
        for(int i=0;i<verticesUtiles.count;i++)
        {
            CGPoint verticePunto=[[verticesUtiles objectAtIndex:i] CGPointValue];
            verticesDibujar[i]=[self convertToNodeSpace:verticePunto];
            
            ccDrawLine([self  ConvierteAMiEje:puntoOrigen],[self ConvierteAMiEje:verticePunto] );
        
                     
     
            
         
        }
        
        
       // ccDrawPoly(verticesDibujar, verticesUtiles.count, YES);

        
        
        
       
    }
 


    
    
}


-(CGPoint) puntoEnLinea:(CGPoint) puntoA puntoB:(CGPoint) puntoB punto:(float) distancia siguiente:(BOOL) siguiente
{
    
    double angulo=atan2(puntoB.y-puntoA.y,puntoB.x-puntoA.x);
    CGPoint puntoResultado;
    
    if (siguiente)
    
        puntoResultado=ccp(puntoB.x+distancia*cos(angulo),puntoB.y+distancia*sin(angulo));  
    
    else {
         puntoResultado =ccp(puntoB.x-distancia*cos(angulo),puntoB.y-distancia*sin(angulo)); 
        
    }
    return puntoResultado;
    
    
    
    
    
}

-(CGPoint) siguienteLinea:(CGPoint) puntoOrigen puntoDestino:(CGPoint) puntoDestino punto:(float) distancia
{
    
    double angulo=atan2(puntoDestino.y-puntoOrigen.y,puntoDestino.x-puntoOrigen.x);
    CGPoint puntoResultado=ccp(puntoDestino.x+distancia*cos(angulo),puntoDestino.y+distancia*sin(angulo));   
    return puntoResultado;
    
    
}

-(void) dibujarRayCast:(CGPoint) puntoOrigen puntoDestino:(CGPoint) puntoDestino angulo:(int) angulo

{
    int paso=20;
    int vuelta=angulo/paso;
     puntoDestino= ccpRotateByAngle(puntoDestino, puntoOrigen,CC_DEGREES_TO_RADIANS(45));
    
    for (int i=0;i<=vuelta;i++)
    {
        
        
        ccDrawLine(puntoOrigen, puntoDestino);
        puntoDestino= ccpRotateByAngle(puntoDestino, puntoOrigen,CC_DEGREES_TO_RADIANS(-paso));
        
    }

}
 
 
                     

-(void) pintarPoligono: (const CGPoint *) poli :( NSUInteger) numberOfPoints :( BOOL) closePolygon 
{
	 
    
	// Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
	// Needed states: GL_VERTEX_ARRAY, 
	// Unneeded states: GL_TEXTURE_2D, GL_TEXTURE_COORD_ARRAY, GL_COLOR_ARRAY	
    
    
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
    
    //glColor4f(0, 255, 0, -100);
    glColor4ub (30, 255, 0, 10);
  
    glVertexPointer(2, GL_FLOAT, 0, poli);
    
    if(closePolygon)
    {
     
        glDrawArrays(GL_TRIANGLE_FAN, 0, numberOfPoints);
    
    }
    else {
        glDrawArrays(GL_LINE_STRIP, 0, numberOfPoints);
    }
    
	
    
	// restore default state
	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_TEXTURE_2D);	
}



    
 


 
 



//////////////////////////// FIN DE LAS FUNCIONES DE RADAR //////////////////
//se deben de actualizar los estados segun los objetos en escena

-(void) actualizarEstados:(ccTime) tiempo objetos:(CCArray *) listaDeObjetos
{
    
    //primero comprobaciones con el mapa
    [self actualizarRadar];
    
   
    
    

}



// se debe comprobar las casillas para ver si esta el heroe/heroes o objetos no traspasables







-(void) Patrullar 


{
    //se pasa un string del formato "0,0 0,2" las coordenadas separadas por espacios y las coordenads por comas
    
    
    
    NSArray *puntosDeLaCadena;
    NSCharacterSet *puntosPatron = [NSCharacterSet characterSetWithCharactersInString: @", "];
    puntosDeLaCadena = [self.rutaPatrulla componentsSeparatedByCharactersInSet:puntosPatron];

    
    //una vez que tenemos los puntos separados , los metemos en un array de puntos
    NSMutableArray *lineaMovimiento=[NSMutableArray array];
    
    CGPoint posicionOrigen;
    posicionOrigen=self.position;

             
    for(int i=0;i<puntosDeLaCadena.count;i++)
    {
        
        CGPoint punto;
        punto.x=[[puntosDeLaCadena objectAtIndex:i] intValue ];
        punto.y=-[[puntosDeLaCadena objectAtIndex:i+1] intValue] ;
        i++;
        
        //se inserta en el array de puntos
        //se convierte a las coordenadas del origen
        
        punto=ccpAdd(posicionOrigen, punto);
        
       //antes de insertar en el array se ajusta al tilse
        punto=[[[COControlador sharedControlador] mapaActual] ajustarPuntoCentroTilset:punto];
        
   
       [lineaMovimiento addObject:[NSValue valueWithCGPoint:punto]];
         
    }
    
    //una vez insertado en el array de puntos creamos las animaciones
    //si el ultimo punto conicide con el primero, se repite el movimiento desde el principio para siempre
    //si no coincide, se hace la ruta contraria y se repite para siempre
    
    CGPoint ultimoPunto=[[lineaMovimiento lastObject] CGPointValue];
    
 
    if (ultimoPunto.x!=posicionOrigen.x || ultimoPunto.y!=posicionOrigen.y)
    {
        NSMutableArray *arrayReverso=[NSMutableArray array];
        
         
        for(id objeto in lineaMovimiento.reverseObjectEnumerator)
        {
            [arrayReverso addObject:objeto];
            
        }
        
    [lineaMovimiento addObjectsFromArray:(NSArray *) arrayReverso];
    
   
    }
    
   [self stopAllActions];
    
    CCSequence *secuencia=[CCSequence actionsWithArray:[self crearAnimacionConPuntos:lineaMovimiento Velocidad:velocidad anguloCorreccion:anguloSprite] ];
    CCRepeatForever *repetir=[CCRepeatForever actionWithAction:secuencia];
    
    [self runAction:repetir];
    



     

  }  
    
-(NSMutableArray *) crearAnimacionConPuntos:(NSMutableArray *) arrayPuntos Velocidad:(float) pVelocidad  anguloCorreccion:(float) anguloDeCorreccion

    {
        //funcion para crear un array de acciones de movimiento mediante  un array de puntos. A una velocidad
        //determinada. Se le pasará también el grado de correccion del sprite, por defecto 180 grados (mirando el sprite
        //a la derecha
        
        NSMutableArray *acciones=[[NSMutableArray alloc] init];
        
        CGPoint posOrigen;
        posOrigen=self.position;
        
        
        
        for(int i=0;i<arrayPuntos.count;i++)
        {
   
            CGPoint paso=[[arrayPuntos objectAtIndex:i] CGPointValue];
            //se crea el angulo de dos puntos para ver la orientacion del sprite
            float angulo;//=ccpAngleSigned(paso, self.position);
            angulo=  CC_RADIANS_TO_DEGREES(- ccpToAngle(ccpSub(posOrigen, paso)));
            //el sprite mira hacia la derecha, por lo que hay que rotarlo para que se calcule
            //los puntos en correspondencia al sprite
            angulo+=anguloDeCorreccion;
            CCAction *rotacion=[CCRotateTo actionWithDuration:0.1 angle:angulo];
            [acciones addObject:rotacion];
            //para mantener la velocidad, el tiempo debe cambiar
            //modulo de la distancia
            float moduloDistancia=ccpDistance(paso, posOrigen);
            //suponiendo una velocidad constante de 10
            float tiempo;
            if(pVelocidad==0)
            {
                 tiempo=0;
                
            }else{
                tiempo=moduloDistancia /pVelocidad;
            }
                
               
        
            //el paso hay que ajustarlo a la posicion en tile
 
            CCAction *accion=[CCMoveTo actionWithDuration:tiempo position:paso];
            [acciones addObject:accion];
            posOrigen=paso;
        }
        
        return acciones;
       
    }
 

-(void) dealloc
{
    [super dealloc];
    
}

@end

