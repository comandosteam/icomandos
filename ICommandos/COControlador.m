//
//  Controlador.m
//  ICommandos
//
//  Created by Noriaf on 19/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "COControlador.h"



@implementation COControlador

static COControlador * _sharedControlador = nil;    
 
@synthesize mapaActual=_mapaActual;

 

 




+(COControlador*)sharedControlador {
    @synchronized([COControlador class])                             
    {
        if(!_sharedControlador)                                     
            [[self alloc] init]; 
        return _sharedControlador;                                  
    }
    return nil; 
}

-(id) init
{
    
    self=[super init];
    
    if(self!=nil)
        
    {
        
    }
    
    
    return self;
    
}


+(id)alloc 
{
    @synchronized ([COControlador class])                            // 5
    {
        NSAssert(_sharedControlador == nil,
                 @"Attempted to allocated a second instance of the Game Manager singleton"); // 6
        _sharedControlador = [super alloc];
        return _sharedControlador;                                 // 7
    }
    return nil;  
}


@end
