//
//  COLayerGame.m
//  ICommandos
//
//  Created by Noriaf on 06/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "COLayerGame.h"
#import "COEnemigo.h"





#define TileMapNode 1
#define Tiempo 2



@implementation COLayerGame


-(id) init

{
    
    self=[super init];
    
    if (self!=nil)
    {
        
     
        
        self.isTouchEnabled=YES;
        tileMap=[[COMapa alloc] initWithMap:@"Mapa.tmx"];
        
        //se lo definimos al controlador
        
       [COControlador sharedControlador].mapaActual=tileMap;
        CCLOG(@"mapa %@", [[COControlador sharedControlador] mapaActual].puntosMediosParedes);
       
        
    
     [self addChild:tileMap z:kPosMapa tag:kTipoObjetoMapa];
        
        
        tileMap.anchorPoint=ccp(0,0);
      
        
        //crea los enemigos
        
        NSString *nombreObjeto=@"Enemigo";
        int numEnemigo=1;
        NSMutableDictionary *enemigoDiccionario;
        
        
        while ((enemigoDiccionario=[[tileMap getEnemigos] valueForKey:[NSString stringWithFormat:@"%@%i",nombreObjeto,numEnemigo]]))
        {    
            COEnemigo *enemigo=[[COEnemigo alloc] initWithDiccionario:enemigoDiccionario] ;
        [self addChild:enemigo z:kPosEnemigo];
        
            //le asignamos el mapa que tiene
             
            
            
            numEnemigo+=1; 
            
        };
        
  
        
 
        player=[CCSprite spriteWithFile:@"heroe.png"];
        player.position=ccp(100,100);
        
        
        [self addChild:player z:kPosPlayer];
     
        
        
       
 
        
        //controlando el layer del mapa
        
         
        control=[[CCPanZoomController controllerWithNode:self] retain];
        control.boundingRect=CGRectMake(0,0, 32*30, 32*30);
        control.zoomOutLimit=control.optimalZoomOutLimit;
        
         
    
      
        [control enableWithTouchPriority:1 swallowsTouches:YES];
        
        
        
        
    
        
          
        
        
        
        
        [self scheduleUpdate];
        
        
        
    }
    
    
    return  self;
    
}

 -(void) update:(ccTime) delta
{
    
    // recuepra los enemigos y los update
    
     for (COObjeto *obj in [self children])
     {
         
         
         if  ([obj isKindOfClass: [COEnemigo class]]) 
         {
         [obj actualizarEstados:delta objetos:[self children]];
                         
         }
         
         
     }
    
 
    
}


 
 - (void)registerWithTouchDispatcher {
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self 
                                                     priority:0 swallowsTouches:YES];    
}


-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event 
{
    
 
     
    CGPoint origen=[tileMap tilePosFromLocation:player.position];
    CGPoint touchLocation=[tileMap locationFromTouch:touch];
  
    
    
    touchLocation=[tileMap tilePosFromLocation:touchLocation];
 
    
    [tileMap caminoRecorrer:origen Hasta:touchLocation Sprite:player];
    
    
  
    return  NO;
    
}

-(void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
       
    

   
}
    
 

-(void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
       
    
}



 
 
@end




