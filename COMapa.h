//
//  COMapa.h
//  ICommandos
//
//  Created by Noriaf on 07/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CCTMXTiledMap.h"
#import "cocos2d.h"


 
#import "Constantes.h"


@interface COMapa : CCTMXTiledMap

{
    float tileMapHeightInPixels;
    float tileMapWidhtInPixels;
    float tileWidh;
    float tileHeight;
    CGSize screenSize;
    kTipoMapa tipoMapa;
    NSMutableArray *puntosMediosParedes;
 
    
    
 
}


@property (readonly) float tileMapHeightinPixels;
@property (readonly) float tileMapWidhtInPixels;
@property (readonly) float tileWidh;
@property (readonly) float tileHeight;
@property (nonatomic,retain) NSMutableArray *puntosMediosParedes;


 
//leer las configuraciones del mapa


-(NSMutableDictionary *) getEnemigos;
-(NSMutableArray *) getPuntosPared;
-(BOOL) isPared:(CGPoint) tile;
 
// raycast de tilset, devuelve el tilset pared primero
-(CGPoint) rayTrace:(CGPoint) tileOrigen tileDestino:(CGPoint) tileDestion;
//dado un rectangulo como vision, devuelve los tilset paredes que se encuentran dentro
-(NSMutableArray *) objetosEnVision: (CGRect) vision;
-(NSMutableArray *) calcularPuntoCorteTilset:(CGPoint) puntoOrigen puntoFinal:(CGPoint) puntoFinal centroTilset:(CGPoint) centroTilset;
-(BOOL) isColision:(CGPoint) tilsetComprobar;
-(NSMutableArray *) objetosAnguloVisionOrigen: (CGPoint)  puntoOrigen puntoDestino :(CGPoint)  puntoDestino angulo:(int) angulo;

//pathfinder en el mapa segun los bloques de colisiones
-(void) caminoRecorrer: (CGPoint) desde Hasta:(CGPoint) hasta Sprite:(CCSprite *) player;


//funciones para posicionarse en el mapa
-(CGPoint) locationFromTouch:(UITouch *) touch;
-(CGPoint) tilePosFromLocation:(CGPoint) location;
-(void) centerTileMapOnTileCoord:(CGPoint) tilePos;
-(CGPoint) locationFromTile:(CGPoint) tileCoor;
-(id) initWithMap:(NSString *) Mapa;
-(CGPoint) setPosicionVista: (CGPoint) position;
-(CGPoint) ajustarPuntoCentroTilset:(CGPoint) location;

@end