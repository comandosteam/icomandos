//
//  COLayerDemo.m
//  ICommandos
//
//  Created by Noriaf on 29/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "COLayerDemo.h"

@implementation COLayerDemo

//se inserta las clases de personajes




-(id) init

{
    
    self=[super init];
    
    if (self!=nil)
    {
        
        
        //inicializamo el array para los puntos
        caminoFinal=[[NSMutableArray alloc]init];
        
        
        CGSize screenSize=[[CCDirector sharedDirector] winSize];
        self.isTouchEnabled=YES;
        
        
        //Inicializamos un enemigo de pruebas con la letra A
        
        Enemigo=[[COEnemigo alloc] initWithFile:@"blocks.png" rect:CGRectMake(0,0,32,32)];
        Enemigo.position=CGPointMake(screenSize.width/2, screenSize.height/2);
        Enemigo.scale=0.5;
        
        [self addChild:Enemigo];
        //inicializamos el heroe con la letra D
        
        Protagonista=[[COHeroe alloc] initWithFile:@"blocks.png" rect:CGRectMake(32,32,32,32)];
        Protagonista.scale=0.5;
        
        [self addChild:Protagonista];
        
        
        Enemigo1=[[COEnemigo alloc] initWithFile:@"blocks.png" rect:CGRectMake(0,0,32,32)];
        Enemigo1.position=CGPointMake(1, screenSize.height/2);
        Enemigo1.scale=0.5;
       
        [self addChild:Enemigo1];
        
        
        
        
        
      
        
        
        
        //creamos el movimiento
        ccColor4B color;
        color=ccc4(255, 255, 255, 255);
        CCRibbon *ribon=[CCRibbon ribbonWithWidth:5 image:@"Icon-Small.png" length:5 color:color fade:2];
        [self addChild:ribon z:1 tag:1];
        
        

        
       
        
        
    }
    
    
    return self;
    
}

-(void) registerWithTouchDispatcher{
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
    
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    
    
    CGPoint touchLocation = [touch locationInView:[touch view]];
    touchLocation = 
    [[CCDirector sharedDirector] convertToGL:touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    //si el toque esta dentro del sprite del protagonista mensaje
    
    
    
     
    
    
    
    //le añadimos un paso
     CCRibbon *ribon=(CCRibbon *) [self getChildByTag:1];
    [ribon addPointAt:touchLocation width:5];
    
     
    
    
    
    //una vez dibujado lo metemos en el array de puntos
    
    [caminoFinal addObject:[NSValue valueWithCGPoint:touchLocation]];
    
       
     
    
    
    
    return YES;
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint touchLocation = [touch locationInView:[touch view]];
    touchLocation = 
    [[CCDirector sharedDirector] convertToGL:touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];
    
    CCRibbon *ribon=(CCRibbon *) [self getChildByTag:1];
    [ribon addPointAt:touchLocation width:5];
    
    
   
    
    //nota que se utiliza el nsvalue para meter los puntos
    CCLOG(@"contenido del camino: %d",caminoFinal.count);
    
    [caminoFinal addObject:[NSValue valueWithCGPoint:touchLocation]];
    
    
    
    
    CCLOG(@"hola");
    
        
}


-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint touchLocation = [touch locationInView:[touch view]];
    touchLocation = 
    [[CCDirector sharedDirector] convertToGL:touchLocation];
    touchLocation = [self convertToNodeSpace:touchLocation];
    //el heroe se mueve y destruimos los eslabones
 
    [Protagonista moverConCadena:caminoFinal];
     
    caminoFinal=nil;
    [caminoFinal release];
    caminoFinal=[[NSMutableArray alloc] init];
    
     
    
    
    
}

-(void) dealloc
{
    [super dealloc];
    caminoFinal=nil;
    Enemigo=nil;
    Protagonista=nil;
    
}

@end
