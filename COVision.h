//
//  COVision.h
//  ICommandos
//
//  Created by Noriaf on 17/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CCNode.h"
#import "cocos2d.h"
 



@interface COVision : CCNode

//la vision del radar debe ser un rectangulo
{
    
    CGRect vision;
    
    //es el centro del enemigo
    CGPoint origenRadar;
    // origen del padre
    
    CGPoint padreOrigen;
    
    
    
    
    NSMutableArray *puntosPared;
    NSMutableArray *puntosConvertidos;
    
    
    
    
}

@property (nonatomic) CGRect vision;
@property (nonatomic) CGPoint origenRadar;
@property (nonatomic) CGPoint padreOrigen;

@property (nonatomic,retain) NSMutableArray *puntosPared;
@property (nonatomic,retain) NSMutableArray *puntosConvertidos;
 



//funcion que configura el radar de vision
-(CGRect) crearRadar;
//para tradar los objetos que ve
-(CGPoint) rayTrace:(CGPoint) tileOrigen tileDestino:(CGPoint) tileDestion;
//para pintar el triangulo fan de la vison
-(void) pintarPoligono: (const CGPoint *) poli :( NSUInteger) numberOfPoints :( BOOL) closePolygon ;
//objetos que pertenecen al rectangulo de la vision
-(NSMutableArray *) objetosEnVision;
//dibuja el radar

-(void) dibujarRadar;
@end

 
