//
//  COVision.m
//  ICommandos
//
//  Created by Noriaf on 17/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "COVision.h"
#import "Constantes.h"
 





@implementation COVision

 
@synthesize vision;
@synthesize origenRadar;
@synthesize puntosPared;
@synthesize puntosConvertidos;
@synthesize padreOrigen;



 


//necesitaremos el ancho y el alto de un tilset para los calculos
//@synthesize mapa;




-(id) init
{
    
    
    self=[super init];
    
    if (self!=nil)
    {

        
    }
    
    
    return self;
    
    
}

-(void) dibujarRadar
{
    
    vision=[self crearRadar];
    
    [self puntosVistos];
    
    
    CGPoint vertices[4]=
    {
        vision.origin,
        ccp(vision.origin.x,vision.size.height),
        ccp(vision.size.width,vision.size.height),
        ccp(vision.size.width,vision.origin.y)
        
        
        
        
    };
    
    //convierto los vertices a la capa
    
   
    
    
    //ccDrawPoly(vertices, 4, YES);
    
    
    
    
    //dibuja una linea a cada objeto
    
    NSMutableArray *objetos=[NSMutableArray array];
    objetos=[self objetosEnVision];
    
    for (int i=0;i<objetos.count;i++)
    {
        CGRect cuadrado=[[objetos objectAtIndex:i] CGRectValue];
        CGPoint puntoMedio=ccp(CGRectGetMidX(cuadrado),CGRectGetMidY(cuadrado));
        
        
        
        CCLOG(@"puntos %@",objetos);
      //  padreOrigen=[self parent].position;
       // padreOrigen=[self convertToNodeSpace:padreOrigen];
     //   puntoMedio=[self convertToNodeSpace:puntoMedio];
        
        
         
         ccDrawLine(   self.position , ccpSub(puntoMedio,[self parent].position));
        
        
        
    }
    
    
    

}

-(void) draw
{
    
    [self dibujarRadar];
    
    //[super draw
   
   // ccDrawLine(visionRadar.origin, ccp(visionRadar.origin.x, visionRadar.size.height));
   /// ccDrawLine(ccp(visionRadar.origin.x, visionRadar.size.height), ccp(visionRadar.size.width,visionRadar.size.height));
   // ccDrawLine(ccp(visionRadar.size.width,visionRadar.size.height), ccp(visionRadar.size.width,visionRadar.origin.y));
   // ccDrawLine(ccp(visionRadar.size.width,visionRadar.origin.y),ccp(visionRadar.origin.x,visionRadar.origin.y));
    
  
    
       
    
}
    

-(void) puntosVistos
{
    
    NSMutableArray *objetosEnRadio=[NSMutableArray array];
    
    // se calcula los vertices
    
    objetosEnRadio=[self objetosEnVision];
    
   
    
    //desde el punto de referencia se calcula los que se ven
    
    
    
    
    
    
    
}
 
 

- (void) convertirCoordenadasPared
{
    
    NSMutableArray *puntos =[NSMutableArray array];
    
    
    for (int i=0;i<puntosPared.count;i++)
    {
        
        CGPoint punto=[[puntosPared objectAtIndex:i] CGPointValue];
        [self convertToNodeSpace:punto];
         
        [puntos addObject:[NSValue valueWithCGPoint:punto]];
         
    }
    
    puntosConvertidos=puntos;
    
    
    

}
-(NSMutableArray *) objetosEnVision
{
    
    //se procesan los puntos de paredes y se convierten en rectangulos
    NSMutableArray *cuadrados=[NSMutableArray array];
     
    int tilSetX=32 ;
    int tilsetY=32 ;
    
    vision=[self crearRadar];
    
    for(int i=0;i<puntosPared.count;i++)
        
    {
        
        //por cada punto medio se calcula el rectangulo
        CGPoint puntoMedio= [[puntosPared objectAtIndex:i] CGPointValue];
        CGPoint origen;
        origen=ccpSub(puntoMedio,ccp(tilSetX/2,tilsetY/2));
        
        //convertimos el origen a nuestro sistema de referencia
       
       //origen=[self convertToNodeSpace:origen];
        
        
        
        CGRect cuadrado=CGRectMake(origen.x, origen.y, tilSetX, tilsetY);
        
        //si hace interseccion con la vision, se coge para procesar
    
        if ( CGRectIntersectsRect(vision, cuadrado) || CGRectContainsRect(vision, cuadrado))
            {
                [cuadrados addObject:[NSValue valueWithCGRect:cuadrado]]; 
            }
        
    }
    
    //una vez creado los cuadrados se comprueban los que estan en vision
    
    

    
    return cuadrados;
    
    
}


-(CGRect) crearRadar
{
    
    CGRect visionRectangulo;
    
    //vision de 3x3
    int tilsetVisionX=3;
    int tilsetVisionY=3; 
    float visionX,visionY;
    visionX=tilsetVisionX*32;
    visionY=tilsetVisionY*32;
    
   
    
    
    //el radar debe estar posicionado donde el paddre
    
   
    padreOrigen=[self parent].position;
    
    
    
    visionRectangulo=CGRectMake(  padreOrigen.x, padreOrigen.y, visionX, visionY);
   
    
    
    
    
    return visionRectangulo;
    
    
}



// necesitaremos el ray Trace para detectar que es una pared
// se manda una posicion en pixeles de la pantalla, convierte a tilset y devuelve el punto medio
// en pixeles del centro de la pared detectada. Si devuelve -1,-1 no ha detectado pared

-(CGPoint) rayTrace:(CGPoint) tileOrigen tileDestino:(CGPoint) tileDestion
{
    
    CGPoint puntoPared=ccp(-1,-1);
    
    
    
    
    
    
    CGPoint delta;
    
    delta.x=abs(tileDestion.x-tileOrigen.x);
    delta.y=abs(tileDestion.y-tileOrigen.y);
    
    
    int x=tileOrigen.x;
    int y=tileOrigen.y;
    int n=1+delta.x+delta.y;
    
    int x_inc=(tileDestion.x>tileOrigen.x) ?1:-1;
    int y_inc=(tileDestion.y>tileOrigen.y) ?1:-1;
    
    
    int error=delta.x-delta.y;
    
    delta.x*=2;
    delta.y*=2;
    
    
    for(;n>0;--n)
    {
        if ((x>=0 && x<=29) && (y>=0 && y<=29))
        {
              //visit(x,y);////provisional
      //         if([mapa isPared:ccp(x,y)])
      //        {
            //si hay pared se manda el tilset de la pared
      //      puntoPared=ccp(x,y);
            //debe devolver el punto centrado de la pared
      //           puntoPared=[mapa locationFromTile:puntoPared];
            
      //      return puntoPared;
            
            // }
        } 
        /////////
        
        if (error>0)
        {
            x+=x_inc;
            error-=delta.y;
            
        }
        else {
            y+=y_inc;
            error+=delta.x;
            
        }
        
        
    }
    
    
    return puntoPared;
    
}

//funcion para pintar un poligono con textura

-(void) pintarPoligono: (const CGPoint *) poli :( NSUInteger) numberOfPoints :( BOOL) closePolygon 
{
    
    
	// Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
	// Needed states: GL_VERTEX_ARRAY, 
	// Unneeded states: GL_TEXTURE_2D, GL_TEXTURE_COORD_ARRAY, GL_COLOR_ARRAY	
    
    
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
    
    //glColor4f(0, 255, 0, -100);
    glColor4ub (30, 255, 0, 10);
    glVertexPointer(2, GL_FLOAT, 0, poli);
    
    if(closePolygon)
    {
        glDrawArrays(GL_TRIANGLE_FAN, 0, numberOfPoints);
        
    }
    else {
        glDrawArrays(GL_LINE_STRIP, 0, numberOfPoints);
    }
    
	
    
	// restore default state
	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_TEXTURE_2D);	
}




-(CGPoint) tilePosFromLocation:(CGPoint) location  
{
    
    float tileMapHeightInPixels=32*30;
    //location=[self convertToNodeSpace:location];
    
    CGPoint pos=  location  ;
    
    pos.x=(int)(pos.x / 32);
    
    pos.y=(int)((tileMapHeightInPixels-pos.y)/32);
    
 
    
    return pos;
    
    
}




@end
