

#import "Constantes.h"
#import "COActor.h"
#import "COMapa.h"
#import "COVision.h"
#import "COControlador.h"






@interface COEnemigo : COActor


{
    kTipoEnemigo tipoEnemigo;
    NSString *rutaPatrulla;
  
    
   // COVision *radar;
 
    //
    
 
    
    
}


@property (nonatomic,retain) NSString *rutaPatrulla;
@property (nonatomic) kTipoEnemigo tipoEnemigo;
 

  

-(CGPoint) puntoEnLinea:(CGPoint) puntoA puntoB:(CGPoint) puntoB punto:(float) distancia siguiente:(BOOL) siguiente;

-(CGPoint) siguienteLinea:(CGPoint) puntoOrigen puntoDestino:(CGPoint) puntoDestino punto:(float) distancia;


-(void) Patrullar;
-(id) initWithDiccionario:(NSMutableDictionary*) ArrayInicializacion;
-(NSMutableArray *) crearAnimacionConPuntos:(NSMutableArray *) arrayPuntos Velocidad:(float) pVelocidad  anguloCorreccion:(float) anguloDeCorreccion;



-(void) actualizarEstados:(ccTime) tiempo objetos:(CCArray *) listaDeObjetos;
 


 
-(void) pintarPoligono: (const CGPoint *) poli :( NSUInteger) numberOfPoints :( BOOL) closePolygon;

 

@end