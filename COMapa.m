//
//  COMapa.m
//  ICommandos
//
//  Created by Noriaf on 07/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "COMapa.h"
#import "AStarPathFinder.h"




@implementation COMapa


@synthesize tileMapHeightinPixels;
@synthesize tileMapWidhtInPixels;
@synthesize tileWidh;
@synthesize tileHeight;
@synthesize puntosMediosParedes;




-(id) initWithMap:(NSString *)Mapa
{
    
    
    self=[super initWithTMXFile:Mapa];
    
    if (self!=nil)
    {
        
    
        screenSize=[[CCDirector sharedDirector] winSize];
        //se actualizan variables para trabajar mas comodamente
        tileMapHeightInPixels=self.mapSize.height*self.tileSize.height;
        tileMapWidhtInPixels=self.mapSize.width*self.tileSize.width;
        tileHeight=self.tileSize.height;
        tileWidh=self.tileSize.width;
        
        puntosMediosParedes=[NSMutableArray array];
        puntosMediosParedes=[self getPuntosPared];
        
      
    }
    
  
    return  self;
}
 
//calcula el punto de corte con un tilset, devuelve los vertices y el punto de corte
-(NSMutableArray *) calcularPuntoCorteTilset:(CGPoint) puntoOrigen puntoFinal:(CGPoint) puntoFinal centroTilset:(CGPoint) centroTilset

{
    
    
    
    CGPoint puntoCorte=ccp(-1,-1);
    int segmentoIndice=-1;
    
     
    // se buscan los vertices del cuadrado
    
    
    CGPoint verticeA,verticeB,verticeC,verticeD;
    float mitadTilsetX=tileWidh/2;
    float mitadTilsetY=tileHeight/2;
    CGRect tilsetComprobar;
    
    
    
    
    //  C ------ D
    //   !      !
    //   !      !
    //  A ------ B
    
    
    verticeA=ccpAdd(centroTilset,ccp(-mitadTilsetX,-mitadTilsetY)) ;    
    verticeB=ccpAdd(centroTilset,ccp(mitadTilsetX,-mitadTilsetY)) ;
    verticeC=ccpAdd(centroTilset,ccp(-mitadTilsetX,mitadTilsetY)) ;
    verticeD=ccpAdd(centroTilset,ccp(mitadTilsetX,mitadTilsetY)) ;
    
    tilsetComprobar=CGRectMake(verticeA.x, verticeA.y, tileWidh, tileHeight);
    
    
    //con los vertices se calcula los puntos de corte
    
    CGPoint puntoCorteAB[3]={verticeA,verticeB,ccpIntersectPoint(puntoOrigen, puntoFinal, verticeA, verticeB)};
    CGPoint puntoCorteBD[3]={verticeB,verticeD,ccpIntersectPoint(puntoOrigen, puntoFinal, verticeB, verticeD)};
    CGPoint puntoCorteDC[3]={verticeD,verticeC,ccpIntersectPoint(puntoOrigen, puntoFinal, verticeD, verticeC)};
    CGPoint puntoCorteAC[3]={verticeA,verticeC, ccpIntersectPoint(puntoOrigen, puntoFinal, verticeA, verticeC)};
    
    CGPoint  *cortesTilset[4]={puntoCorteAB,puntoCorteAC,puntoCorteBD,puntoCorteDC};
    
    
    
    //el punto de corte debe estar dentro de los limites del cuadrado y es el de distancia mas cercana;
    
    float distancia=100000;
    
    for (int i=0;i<3;i++)
    {
        CGPoint punto= cortesTilset[i][2];
        
        BOOL estaEnCuadrado=CGRectContainsPoint(tilsetComprobar, punto);
        
        if (ccpDistance(cortesTilset[i][2], puntoOrigen)<=distancia && estaEnCuadrado)
        {
            puntoCorte=punto;
            distancia=ccpDistance(puntoCorte, puntoOrigen);
            segmentoIndice=i;
        }
    }
    
    
    if(segmentoIndice!=-1){ 
        
        NSMutableArray *vertices=[NSMutableArray array];
        for(int i=0;i<3;i++)
        {
            [vertices addObject:[NSValue valueWithCGPoint:cortesTilset[segmentoIndice][i]]];
            
        }
        return vertices;
        
    }
    
    
    return NULL;
    
    
}








//dado un rectangulo devuelve los puntos que estan en vision

-(NSMutableArray *) objetosAnguloVisionOrigen: (CGPoint)  puntoOrigen puntoDestino :(CGPoint)  puntoDestino angulo:(int) angulo

{


//la idea es mediante una linea empezar a rotarla hasta el angulo correspondiente y comprobar todos los objetos que chocan
//con el raycast, devuelve los centros medios de las colisiones
// el punto destino se asume
    
    NSMutableArray *Colisiones=[[NSMutableArray alloc]init];
   //convierte las coordenadas en tilset
    
    CGPoint puntoOrigenTilset,puntoDestinoTilset;
    CGPoint colisionTilset,colision;
    
    puntoOrigenTilset=[self tilePosFromLocation:puntoOrigen];
    int paso=20;
    int vuelta=angulo/paso;
    
//se pasa a 45 grados hacia arriba
     puntoDestino= ccpRotateByAngle(puntoDestino, puntoOrigen,CC_DEGREES_TO_RADIANS(45));
    
    for (int i=0;i<=vuelta;i++)
    {
        
        
        puntoDestinoTilset=[self tilePosFromLocation:puntoDestino];
        
         colisionTilset=[self rayTrace:puntoOrigenTilset tileDestino:puntoDestinoTilset];
        
        if(!CGPointEqualToPoint(colisionTilset, ccp(-1,-1)))
       {
           //es una colision, por lo que se pasan a coordenadas normales y se añade al array
           colision=[self locationFromTile:colisionTilset];
           [Colisiones addObject:[NSValue valueWithCGPoint:colision]];
    
       }
        
     //una vez comprobado, se actualiza el angulo y se comprueba con el siguiente
     //en el sentido de las agujas del reloj, por eso -1
        
        puntoDestino= ccpRotateByAngle(puntoDestino, puntoOrigen,CC_DEGREES_TO_RADIANS(-paso));
    
    }
       

    
    return Colisiones;
    
}

-(NSMutableArray *) objetosEnVision: (CGRect) vision
{
    
    //se procesan los puntos de paredes y se convierten en rectangulos
    NSMutableArray *cuadrados=[[NSMutableArray alloc]init];
  
    
    
    for(int i=0;i<self.puntosMediosParedes.count;i++)
        
    {
        
        //por cada punto medio se calcula el rectangulo
        CGPoint puntoMedio= [[self.puntosMediosParedes objectAtIndex:i] CGPointValue];
        CGPoint origen;
        origen=ccpSub(puntoMedio,ccp(self.tileWidh/2,self.tileHeight/2));
        
        //convertimos el origen a nuestro sistema de referencia
        
       
        
        
        
        CGRect cuadrado=CGRectMake(origen.x, origen.y, self.tileWidh, self.tileHeight);
        
        //si hace interseccion con la vision, se coge para procesar
        
        if ( CGRectIntersectsRect(vision, cuadrado)  )
        {
            [cuadrados addObject:[NSValue valueWithCGRect:cuadrado]]; 
        }
        
    }
    
    //una vez creado los cuadrados se comprueban los que estan en vision
    
   
    
    return cuadrados;
    
    
}

-(CGPoint) rayTrace:(CGPoint) tileOrigen tileDestino:(CGPoint) tileDestion
{
    
    CGPoint puntoPared=ccp(-1,-1);
    
     //convierte los puntos a tilset
    

    
    
    CGPoint delta;
    
    delta.x=abs(tileDestion.x-tileOrigen.x);
    delta.y=abs(tileDestion.y-tileOrigen.y);
    
    
    int x=tileOrigen.x;
    int y=tileOrigen.y;
    int n=1+delta.x+delta.y;
    
    int x_inc=(tileDestion.x>tileOrigen.x) ?1:-1;
    int y_inc=(tileDestion.y>tileOrigen.y) ?1:-1;
    
    
    int error=delta.x-delta.y;
    
    delta.x*=2;
    delta.y*=2;
    
    
    for(;n>0;--n)
    {
        if ((x>=0 && x<=29) && (y>=0 && y<=29))
        {
            // visit(x,y);////provisional
            // if([self isColision:ccp(x,y)])
             
              if([puntosMediosParedes containsObject:[NSValue valueWithCGPoint:[self locationFromTile:ccp(x,y)]]])
             {
                //si hay pared se manda el tilset de la pared
                 puntoPared=ccp(x,y);
                
                
                
                 return puntoPared;
                
              }
        } 
        /////////
        
        if (error>0)
        {
            x+=x_inc;
            error-=delta.y;
            
        }
        else {
            y+=y_inc;
            error+=delta.x;
            
        }
        
        
    }
    
    
    
    
    
    
    return puntoPared;
    
}

-(CGPoint) ajustarPuntoCentroTilset:(CGPoint) location  
{
   
    //se calcula el tile que es, y hay que recalcular el punto medio de ese tile
    
    CGPoint pos= ccpSub(location,ccp(0,0));
    
    pos.x=(int)(pos.x / self.tileWidh);
    
    pos.y=(int)(pos.y/self.tileHeight);
    
    
    //el punto medio es el numero de tile-1 mas la mitad del tile
    
    pos.x=((pos.x)*self.tileWidh)+self.tileWidh/2;
    pos.y=( (pos.y)*self.tileHeight)+self.tileHeight/2;
    
    
    
    
    return pos;
    
}


 



-(NSMutableDictionary *) getObjetosTipo:(NSString *) tipoObjeto capa:(NSString *) nomCapa
{

    
    // Los enemigos tendran el objeto con el nombre Enemigo(N) donde n es un secuencial de enemigos
    // por cada enemigo tendremos una X e Y para localizarlo como punto de origen
    // se recupera un array con los puntos de la ruta a hacer
    // propiedades de enemigos: TipoEnemigo (por ahora solo implementado kSoldado)
    
    
    
    NSMutableDictionary *ObjetosDelMapa=[[NSMutableDictionary alloc] init] ;
    
    //se definen el grupo de objetos
    CCTMXObjectGroup *Objetos = [self objectGroupNamed:nomCapa];
    NSAssert(Objetos != nil, @"la capa de objetos no se ha encontrado");
    
    //los enemigos tendran la forma de ENemigo N
    
    NSString *nombreObjeto=tipoObjeto;
    int numObjeto=1;
    
    
    
    NSMutableDictionary *objetoMapa;
    
    
    while ((objetoMapa = [Objetos objectNamed:[NSString stringWithFormat:@"%@%i",nombreObjeto,numObjeto]]))
    {    
        [ObjetosDelMapa setValue:objetoMapa forKey:[NSString stringWithFormat:@"%@%i",nombreObjeto,numObjeto]];
        numObjeto+=1; 
        
    };
    

    return ObjetosDelMapa;
    
  
    


}



-(NSMutableArray *) getPuntosPared
{
    
    NSMutableArray *puntosPared=[[NSMutableArray alloc] init];
    CCTMXLayer *layerColisiones = [self layerNamed:@"Eventos"];
    NSString *isPared=@"isPared";
    NSString *valorColision=@"1";
    
    for(int tilsetX=0;tilsetX<self.mapSize.width;tilsetX++)
    {
        
        for(int tilsetY=0;tilsetY<self.mapSize.height;tilsetY++)
        {
            
            UInt32 tileGid = [layerColisiones tileGIDAt:ccp(tilsetX,tilsetY)];  
            if (tileGid)
            {
                // If a tile exists, see if collide is enabled on the entire layer.
                NSDictionary *ldict = [layerColisiones propertyNamed:isPared];
                if (ldict)
                
                    [puntosPared addObject:[NSValue valueWithCGPoint:[self locationFromTile:ccp(tilsetX,tilsetY)]]];
                
                
                // If not, then check the tile for the collide property.
                NSDictionary *dict = [self propertiesForGID:tileGid];
                if (dict)
                {
                    NSString *collide = [dict valueForKey:isPared];
                    if (collide && [collide compare:valorColision] == NSOrderedSame)
                       [puntosPared addObject:[NSValue valueWithCGPoint:[self locationFromTile:ccp(tilsetX,tilsetY)]]];
                    
                }
            }
            

            
        }
        
        
        
    }
    
    
    
        
    return puntosPared;
    
}




-(BOOL) isColision:(CGPoint) tilsetComprobar
{
    
    BOOL isColision=NO;
    int numElementos=0;
    
     
     
    
    while (!isColision && numElementos<puntosMediosParedes.count) {
        
        CGPoint puntoPared=[[puntosMediosParedes objectAtIndex:numElementos] CGPointValue];
        //lo calcula en tilset
        
        puntoPared=[self tilePosFromLocation:puntoPared];
        
        if(CGPointEqualToPoint(tilsetComprobar, puntoPared))
        {
            //si son iguales , es una pared
            isColision=YES;
            
        }
        
        numElementos++;
        
        
    }
    
    return isColision;
    
    
    
}



-(BOOL) isPared:(CGPoint) tile
{
    CCTMXLayer *layerColisiones = [self layerNamed:@"Eventos"];
    NSString *isPared=@"isPared";
    NSString *valorColision=@"1";
    
    UInt32 tileGid = [layerColisiones tileGIDAt:tile];  
    if (tileGid)
    {
        // If a tile exists, see if collide is enabled on the entire layer.
        NSDictionary *ldict = [layerColisiones propertyNamed:isPared];
        if (ldict)
            return YES;
        
        // If not, then check the tile for the collide property.
        NSDictionary *dict = [self propertiesForGID:tileGid];
        if (dict)
        {
      NSString *collide = [dict valueForKey:isPared];
            if (collide && [collide compare:valorColision] == NSOrderedSame)
                return YES;
        }
    }

    
    return NO;
    
    
}



-(NSMutableDictionary *) getEnemigos
{
    
    //los enemigos son objetos
    
    return [self getObjetosTipo:@"Enemigo" capa:@"Objetos"];
    
}



//camino a recorrer, debe devolver un array con las coordenadas

-(void) caminoRecorrer:(CGPoint)desde Hasta:(CGPoint) hasta Sprite:(CCSprite *) player 
{
    
 
    
    AStarPathFinder *pathFinder = [[AStarPathFinder alloc]
                                  initWithTileMap:self groundLayer:@"Eventos"];
  
    // Optionally, you can set the name of the collide property key and the value it expects.
    [pathFinder setCollideKey:@"isPared"]; // defaults to COLLIDE
    [pathFinder setCollideValue:@"1"]; // defaults to 1
   
    // move a sprite
    [pathFinder moveSprite:player from:desde to:hasta atSpeed:0.1f];
    
 
    
}

 

-(CGPoint) locationFromTile:(CGPoint) tileCoor 
{
    
    
    CGPoint pos;
    
    //tileCoor.x=tileMap.mapSize.width;
    tileCoor.y=self.mapSize.height-tileCoor.y-1;
    
    
    pos.x= (self.tileSize.width*tileCoor.x)+self.tileSize.width/2;
    pos.y= (self.tileSize.height*tileCoor.y)+self.tileSize.height/2;
    
    
    return pos;
    
}



-(void) centerTileMapOnTileCoord:(CGPoint) tilePos
{
    
    
    //centrar el mapa
    
     
    CGPoint screenCenter=ccp(screenSize.width/2,screenSize.height/2);
    
    tilePos.y=(self.mapSize.height-1)-tilePos.y;
    
    CGPoint scrollPosition=CGPointMake(-(tilePos.x*self.tileSize.width),-(tilePos.y*self.tileSize.height));
    
    scrollPosition.x+=screenCenter.x-self.tileSize.width*0.5f;
    scrollPosition.y+=screenCenter.y-self.tileSize.height*0.5f;
    
    scrollPosition.x=MIN(scrollPosition.x,0);
    scrollPosition.x=MAX(scrollPosition.x,-screenSize.width);
    
    
    
    
    scrollPosition.y=MIN(scrollPosition.y,0);
    scrollPosition.y=MAX(scrollPosition.y,-(tileMapHeightInPixels- screenSize.height));
    
    //scrollPosition.y=MAX(scrollPosition.y,- screenSize.height);
    
    
     CCAction *move=[CCMoveTo actionWithDuration:0.2F position:scrollPosition];
    [self stopAllActions];
    [self runAction:move]; 
    
    
    
  
}


-(CGPoint) setPosicionVista: (CGPoint) tilePos
{
    
    
   
    
    CGPoint screenCenter=ccp(screenSize.width/2,screenSize.height/2);
    
    tilePos.y=(self.mapSize.height-1)-tilePos.y;
    
    CGPoint scrollPosition=CGPointMake(-(tilePos.x*self.tileSize.width),-(tilePos.y*self.tileSize.height));
    
    scrollPosition.x+=screenCenter.x-self.tileSize.width*0.5f;
    scrollPosition.y+=screenCenter.y-self.tileSize.height*0.5f;
    
    scrollPosition.x=MIN(scrollPosition.x,0);
    scrollPosition.x=MAX(scrollPosition.x,-screenSize.width);
    
    
    
    
    scrollPosition.y=MIN(scrollPosition.y,0);
    scrollPosition.y=MAX(scrollPosition.y,-(tileMapHeightInPixels- screenSize.height));
    
    return scrollPosition;
    
    
}

 -(CGPoint) locationFromTouch:(UITouch *) touch
{
    CGPoint touchLocation=[touch locationInView:[touch view]];
    //return  [[CCDirector sharedDirector] convertToGL:touchLocation];
    touchLocation=[[CCDirector sharedDirector] convertToGL:touchLocation];
    return  [self convertToNodeSpace:touchLocation];
}

-(CGPoint) tilePosFromLocation:(CGPoint) location  
{
    
    
    //location=[self convertToNodeSpace:location];
    
    CGPoint pos=ccpSub(location,self.position);
    
    pos.x=(int)(pos.x / self.tileSize.width);
    
    pos.y=(int)((tileMapHeightInPixels-pos.y)/self.tileSize.height);
    
    
    
  //   NSAssert(pos.x>=0 && pos.y>=0 &&  pos.x<self.mapSize.width && pos.y<self.mapSize.height,
    //          @" coordenadas fuera de rango" );
    
    return pos;
    
    
}


 

 


- (void)dealloc
{
	
	[super dealloc];
}



@end


